from django.contrib.auth.models import User
from django.db import models


# Create your models here.
class Product(models.Model):
    Title = models.CharField(max_length=50)
    Description = models.TextField()
    Image = models.ImageField()
    Price = models.FloatField()
    def __str__(self):
        return self.Title


STATUS_CHOICES = (
    ('new', 'new'),
    ('paid', 'paid')
)

MODE_OF_PAYMENT = (
    ('cash', 'cash'),
    ('paytm', 'paytm'),
    ('card', 'card')
)


class Order(models.Model):
    UserId = models.ForeignKey(User, on_delete=models.CASCADE)
    Total = models.IntegerField()
    Createdon = models.DateField(auto_now_add=True)
    Updatedon = models.DateField(auto_now=True)
    Status = models.CharField(choices=STATUS_CHOICES, max_length=5)
    mode_of_payment = models.CharField(choices=MODE_OF_PAYMENT, max_length=10)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)

    def __str__(self):
        return self.product.Title


class OrderItem(models.Model):
    OrderId = models.ForeignKey(Order, on_delete=models.CASCADE)
    ProductId = models.ForeignKey(Product, on_delete=models.CASCADE)
    Quantity = models.IntegerField()
    Price = models.FloatField()

    def __str__(self):
        return self.ProductId.Title
